detected_os := $(shell uname)

ifeq ($(detected_os), Darwin)
	# Can only write to /usr/local on macOS due to SIP
	bin_dir := /usr/local/bin
	share_dir := /usr/local/share
else
	bin_dir := /usr/bin
	share_dir := /usr/share
endif

install: snakesay.py fortunes.txt
	install fortunes.txt $(share_dir)/fortunes.txt
	install -m 755 snakesay.py $(bin_dir)/snakesay
clean:
	rm -rf $(share_dir)/fortunes.txt
	rm -rf $(bin_dir)/snakesay.py
